from snakemake.remote import FTP

FTP = FTP.RemoteProvider()

rule all:
    input:
         expand("reads/smallrna/SRR06098{sid}.fastq.gz", sid=[1,2,3,4,5,6]),
         'ref/ncRNA/ncRNA.list.yaml',
         'fastq_screen.config',
         dynamic("index/bowtie2/bowtie2_index_ready.{key}"),
         "ref/miRNA/miRBase_main_mature.fasta",
         "ref/miRNA/miRBase_main_precursors.fasta",
         "ref/miRNA/miRBase_other_mature.fasta",
         "ref/miRNA/miRBase_other_precursors.fasta",
         expand("ref/ucsc/{release}/{release}.fasta",
                release=config.get('omics_data').get('ucsc').get('release'))



include_prefix="rules"

include:
    include_prefix + "/functions.py"
include:
    include_prefix + "/ncrna.smk"
include:
    include_prefix + "/reads.smk"
include:
    include_prefix + "/mirbase.smk"
include:
    include_prefix + "/genomes.smk"