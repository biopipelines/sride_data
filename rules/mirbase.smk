rule get_miRNA_from miRBase:
    input:
        mature=FTP.remote(expand("ftp://mirbase.org/pub/mirbase/{release}/mature.fa.gz",
                                 release=config.get('omics_data').get('mirbase').get('release')), immediate_close=True),
        hairpin=FTP.remote(expand("ftp://mirbase.org/pub/mirbase/{release}/hairpin.fa.gz",
                                 release=config.get('omics_data').get('mirbase').get('release')), immediate_close=True)
    output:
        mature="ref/mirbase/mature.fa.gz",
        hairpin="ref/mirbase/hairpin.fa.gz"
    shell:
        "mv {input.mature} {output.mature} "
        "&& mv {input.hairpin} {output.hairpin} "

rule extract_main_mirna:
    input:
        mature="ref/mirbase/mature.fa.gz",
        hairpin="ref/mirbase/hairpin.fa.gz"
    output:
        mature="ref/miRNA/miRBase_main_mature.fasta",
        hairpin="ref/miRNA/miRBase_main_precursors.fasta"
    params:
        main_organism=config.get("omics_data").get("mirbase").get("main_organism")
    shell:
        "perl rules/scripts/extract_miRNAs.pl "
        "{input.mature} {params.main_organism} > {output.mature} "
        "&& perl rules/scripts/extract_miRNAs.pl "
        "{input.hairpin} {params.main_organism} > {output.hairpin} "

rule extract_other_mirna:
    input:
        mature="ref/mirbase/mature.fa.gz",
        hairpin="ref/mirbase/hairpin.fa.gz"
    output:
        mature="ref/miRNA/miRBase_other_mature.fasta",
        hairpin="ref/miRNA/miRBase_other_precursors.fasta"
    params:
        other_organism=config.get("omics_data").get("mirbase").get("other_organism")
    shell:
        "perl rules/scripts/extract_miRNAs.pl "
        "{input.mature} {params.other_organism} > {output.mature} "
        "&& perl rules/scripts/extract_miRNAs.pl "
        "{input.hairpin} {params.other_organism} > {output.hairpin} "
