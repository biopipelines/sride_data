from collections import Counter
import yaml

c = Counter()

with open(snakemake.input[0], 'r') as f:
    while True:
        line = f.readline()
        if "" == line:
            break;
        if line.startswith('>'):
            header = line
            ncRNA_type = header.split('gene_biotype:')[1].split(' ')[0]
            c[ncRNA_type] += 1

data = dict()
data['ncRNA'] = dict()
for k in c:
    data['ncRNA'][k] = c[k]

with open(snakemake.output[0], 'w') as fn:
    yaml.dump(data, fn, default_flow_style=False)

