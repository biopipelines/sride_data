
sd = '__snakemake_dynamic__'
with open(snakemake.input[0], 'r') as f:
    body = []
    while True:
        line = f.readline()
        if "" == line:
            if body:
                with open(snakemake.output[0].replace(sd, ncRNA_type), 'a') as f2:
                    for i in body:
                        f2.write("{}\n".format(i))            
            break;
        if line.startswith('>'):
            if body:
                with open(snakemake.output[0].replace(sd, ncRNA_type), 'a') as f2:
                    for i in body:
                        f2.write("{}\n".format(i))
            header = line
            body = []
            ncRNA_type = header.split('gene_biotype:')[1].split(' ')[0]
            with open(snakemake.output[0].replace(sd, ncRNA_type), 'a') as f1:
                f1.write("{}".format(header))
        else:
            body.append(line.strip())
