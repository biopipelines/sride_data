import os
from collections import Counter


def search_binary_path(name, path=None, exts=('',)):
    """ http://code.activestate.com/recipes/52224/
    Search PATH for a binary.
    Args:
    name: the filename to search for
    path: the optional path string (default: os.environ['PATH')
    exts: optional list/tuple of extensions to try (default: ('',))

    Returns:
    The abspath to the binary or None if not found.
    """
    path = path or os.getenv('PATH')
    for dir in path.split(os.path.pathsep):
        for ext in exts:
            binpath = os.path.join(dir, name) + ext
            if os.path.exists(binpath):
                return os.path.abspath(binpath)
    return None


c = Counter()

with open(snakemake.input[0], 'r') as f:
    while True:
        line = f.readline()
        if "" == line:
            break;
        if line.startswith('>'):
            header = line
            ncRNA_type = header.split('gene_biotype:')[1].split(' ')[0]
            c[ncRNA_type] += 1

data = dict()
data['ncRNA'] = dict()
for k in c:
    data['ncRNA'][k] = c[k]

with open(snakemake.output[0], 'w') as fl:
    for k in c:
        fl.write('DATABASE\t{}\t../data/index/bowtie2/{}\tBOWTIE2\n'.format(k, k))
    fl.write("BOWTIE2\t{}\n".format(search_binary_path('bowtie2')))

