import multiprocessing
import os


def cpu_count():
    return multiprocessing.cpu_count()


def pipeline_cpu_count(reserve_cpu=2):
    cpu_nums = cpu_count() if reserve_cpu > cpu_count() \
        else cpu_count() - reserve_cpu
    return cpu_nums


def references_abs_path(ref='references'):
    references = config.get(ref)
    basepath = references['basepath']
    provider = references['provider']
    release = references['release']

    return [os.path.join(basepath, provider, release)]


def resolve_single_filepath(basepath, filename):
    return [os.path.join(basepath, filename)]


def get_references_label(ref='references'):
    references = config.get(ref)
    provider = references['provider']
    genome = references['release']

    return '_'.join([provider, genome])


def search_binary_path(name, path=None, exts=('',)):
    """ http://code.activestate.com/recipes/52224/
    Search PATH for a binary.
    Args:
    name: the filename to search for
    path: the optional path string (default: os.environ['PATH')
    exts: optional list/tuple of extensions to try (default: ('',))

    Returns:
    The abspath to the binary or None if not found.
    """
    path = path or os.getenv('PATH')
    for dir in path.split(os.pathsep):
        for ext in exts:
            binpath = os.path.join(dir, name) + ext
            if os.path.exists(binpath):
                return os.path.abspath(binpath)
    return None
