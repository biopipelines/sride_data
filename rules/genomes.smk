rule get_genome_from_UCSC:
    input:
        genome=FTP.remote("ftp://hgdownload.cse.ucsc.edu/goldenPath/{release}/bigZips/{release}.2bit", immediate_close=True)
    output:
        genome="ref/ucsc/{release}/{release}.fasta"
    conda:
        "envs/ucsc-twobittofa.yaml"
    shell:
        "twoBitToFa {input.genome} {output.genome}"