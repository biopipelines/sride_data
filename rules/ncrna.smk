# Gene Predictions label as described in ftp://ftp.ensembl.org/pub/current_README
predictions = "ncrna"

rule get_Ensembl_ncRNA:
    input:
        FTP.remote(expand("ftp://ftp.ensembl.org/pub/{release}/{species}/{predictions}/{filename}{zip_ext}",
                          release=config.get('omics_data').get('ensembl').get('release'),
                          species=config.get('omics_data').get('ensembl').get('species'),
                          predictions=predictions,
                          filename=config.get('omics_data').get('ensembl').get('filename'),
                          zip_ext=config.get('omics_data').get('ensembl').get('zip_ext'),
                         ), immediate_close=True
                   )
    output:
        temp("ref/ensembl/{release}/{species}/{predictions}/{filename}")
    shell:
        "gunzip -c {input} > {output}"

rule split_Ensembl_ncRNA:
    input:
        expand("ref/ensembl/{release}/{species}/{predictions}/{filename}",
               release=config.get('omics_data').get('ensembl').get('release'),
               species=config.get('omics_data').get('ensembl').get('species'),
               predictions=predictions,
               filename=config.get('omics_data').get('ensembl').get('filename')
               )
    output:
        dynamic("ref/{predictions}/{predictions}.{key}.fa")
    script:
        "scripts/ncrna_fasta_splitter.py"

rule count_Ensembl_ncRNA:
    input:
        expand("ref/ensembl/{release}/{species}/{predictions}/{filename}",
               release=config.get('omics_data').get('ensembl').get('release'),
               species=config.get('omics_data').get('ensembl').get('species'),
               predictions=predictions,
               filename=config.get('omics_data').get('ensembl').get('filename')
               )
    output:
        'ref/{predictions}/{predictions}.list.yaml'
    script:
        "scripts/ncrna_fasta_counter.py"

rule create_fastq_screen_config:
    input:
        expand("ref/ensembl/{release}/{species}/{predictions}/{filename}",
               release=config.get('omics_data').get('ensembl').get('release'),
               species=config.get('omics_data').get('ensembl').get('species'),
               predictions=predictions,
               filename=config.get('omics_data').get('ensembl').get('filename')
               )
    output:
        'fastq_screen.config'
    conda:
        "envs/bowtie2.yaml"
    script:
        "scripts/fastq_screen_config.py"

rule bowtie2_build_index:
    input:
        "ref/ncRNA/ncRNA.{key}.fa"
    output:
        touch("index/bowtie2/bowtie2_index_ready.{key}")
    shadow: "shallow"
    conda:
        "envs/bowtie2.yaml"
    params:
        label="{key}"
    log: "logs/bowtie2/build_index.{key}.log"
    threads: pipeline_cpu_count()
    shell:
        "bowtie2-build "
        "--threads {threads} "
        "{input} {params.label} "
        "&>{log} "
        "&& find ./ -name '{params.label}.*.bt2' -type f -print0 | " \
        "xargs -0 -I file mv file index/bowtie2 "
        "&& rm -f {input} "

