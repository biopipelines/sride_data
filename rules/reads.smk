
rule smallrna_reads:
    input:
        FTP.remote("ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR060/SRR06098{sid}/SRR06098{sid}.fastq.gz", immediate_close=True)
    output:
        "reads/smallrna/SRR06098{sid}.fastq.gz"
    shell:
        "mv {input} {output} "
