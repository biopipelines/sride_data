# sRIde data
Snakemake workflow to produce reference and test data for smallRNA-Seq
identification pipeline

## Authors

* Gianmauro Cuccuru

## Data produced

* __ref/mirbase__ miRBase mirror, specify the release number in the _config
.yaml_ file
* __ref/miRNA__ mature and hairpin miRNA by species (hsa, mmu)
* __ref/ncRNA__ non-coding RNAs from [ensembl](http://www.ensembl.org/info/genome/genebuild/ncrna.html)
* __reads/smallrna__ test datasets from [Study: PRJNA131879](https://www.ebi.ac.uk/ena/data/view/PRJNA131879)
* __index/bowtie2__ ncRNA bowtie2 index by types
* __fastq_screen.config__ configuration file for fastq_screen


## Usage
### Execute workflow

Configure the workflow according to your needs via editing the file
_config.yaml_.

Test your configuration by performing a dry-run via

    snakemake --use-conda --configfile config.yaml --dryrun

Execute the workflow locally via

    snakemake --use-conda --configfile config.yaml --cores $N

using `$N` cores

More details at [Snakemake documentation](https://snakemake.readthedocs.io).